Mercari Code Interview 
=========================
Mercari Code Interview Application, it's an application writing in kotlin in order to show, test my solution and have feedback for the scramble test of mercari and have some hints or feedback about
what i have done right and bad.

Introduction
------------

So the test consist of checking if a string s2 in inside a scrumble string s1 :

if string s1 = "aze"
and string s2 = "za"

we can see that s2 is inside s1


What i have done
----------------

For this test basically i created two hashmap one for s1 and one for s2, both of hashmap contains a character key and a counter of the occurrences of this character in the string as value.
if we take the previous example, we have :

for s1 :
    a   -> 1
    z   -> 1
    e   -> 1
    
and for s2 :
    z   -> 1
    a   -> 2
    
And to resolve this problem what we do it's check if all characters of s2 exist in s1 by checking by iteration if a character exist and if s1 have all the occurences of this character

I think time complexity is O(n) because we only doing O(n) operations

You can find the tests in the class ScrambleUnitTest and the implementation of the solution in the class GetScrambleUseCase

Goals
---------
I made this project to be sure that my solution is a correct solution, to understand if it's not correct and to know if there is any better solution and how we can implement this solution.
It will help me to understand better and to be better in resolving more and more complex computer science algorithm.

I think that i will make a clone of this test app, add and implement many other algorithms and make them testable and if i have time or someone could help me i will add some visualisation chart of those algorithms.

Understanding
---------
If my solution is not correct or have bad performance or if you have better solution or just want to give me some hints, you can add notes in the file called notes, change my implementation and makes a pull request.
If you have any question don't hesitate to ask me.

Thanks
---------
I would like to thank Prateek Khandelwal of Mercari who did this interview with me.

