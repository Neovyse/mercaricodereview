package com.mercari.mercaricodeinterview

import com.mercari.mercaricodeinterview.domain.usecases.GetScrambleUseCase
import org.junit.Test

/**
 * Unit tests for scramble algorithm
 */
class ScrambleUnitTest {

    private val scrambleUseCase = GetScrambleUseCase()

    /**************************************** test simple kotlin **********************************/

    /**
     * Should return true
     */
    @Test
    fun testScrambleEmpty() {
        assert(scrambleUseCase.getScramble("", ""))
    }

    /**
     * Should return false
     */
    @Test
    fun testFalseScramble() {
        assert(!scrambleUseCase.getScramble("test", "tttt"))
    }

    /**
     * Should return true
     */
    @Test
    fun testCorrectScramble() {
        assert(scrambleUseCase.getScramble("keetarpmercari", "prateek"))
    }

    /**
     * Should return false
     */
    @Test
    fun testS2LengthGreaterThanS1Length() {
        assert(!scrambleUseCase.getScramble("zzz", "zzzzz"))
    }

    /*********************************************************************************************/

    /**
     * Should return true
     */
    @Test
    fun testScrambleEmptyKotlin() {
        assert(scrambleUseCase.getScramble2("", ""))
    }

    /**
     * Should return false
     */
    @Test
    fun testFalseScrambleKotlin() {
        assert(!scrambleUseCase.getScramble2("test", "tttt"))
    }

    /**
     * Should return true
     */
    @Test
    fun testCorrectScrambleKotlin() {
        assert(scrambleUseCase.getScramble2("keetarpmercari", "prateek"))
    }

    /**
     * Should return false
     */
    @Test
    fun testS2LengthGreaterThanS1LengthKotlin() {
        assert(!scrambleUseCase.getScramble2("zzz", "zzzzz"))
    }

}