package com.mercari.mercaricodeinterview.domain.usecases

import com.mercari.mercaricodeinterview.domain.entities.Algorithm

/**
 * Use case use to retrieved algorithm type
 */
class GetAlgorithmsUseCase() {

    private var algorithms: List<Algorithm> = arrayListOf(Algorithm.SCRAMBLE)


    fun getAlgorithms() = algorithms
}