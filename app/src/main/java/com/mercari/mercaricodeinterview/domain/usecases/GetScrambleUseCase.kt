package com.mercari.mercaricodeinterview.domain.usecases

/**
 * GetScrambleUseCase used to execute scramble algorithm
 */
class GetScrambleUseCase {


    /********************* Algorithm like in the interview, i write it just in simple kotlin ******/

    /**
     * Scramble function use to determine if a string s2 is inside a string s1
     * We supposed that if s2 is empty and s1 is empty it's return true
     * @param s1 scramble string
     * @param s2 string to compare
     * @return true is s1 contains s2 or false
     * Time complexity O(n) we do only addition of O(n)
     */
    fun getScramble(s1: String, s2: String): Boolean {

        if (s2.length > s1.length) {
            return false
        }
        val s1HashMap = convertStringToHashMapCounter(s1)
        val s2HashMap = convertStringToHashMapCounter(s2)
        for (char in s2HashMap.keys) {
            if (s1HashMap[char] == null) {
                return false
            }

            if(s1HashMap[char]!! < s2HashMap[char]!!) {
                return false
            }
        }
        return true

    }

    /**
     * Function who convert string to a hashMap of character and number of occurrences of this
     * character
     * @param string to convert to hashmap
     * @param s2 string to compare
     * @return true is s1 contains s2 or false
     */
    private fun convertStringToHashMapCounter(string: String): HashMap<Char, Int> {
        val sHashMap = HashMap<Char, Int>()
        for (char in string) {
            if (sHashMap[char] == null) {
                sHashMap[char] = 1
            } else {
                sHashMap[char] = sHashMap[char]!! + 1
            }
        }
        return sHashMap
    }

    /**********************************************************************************************/


    /********************* Algorithm like in the interview, just in kotlin to avoid !! *********/

    /**
     * Scramble function use to determine if a string s2 is inside a string s1
     * We supposed that if s2 is empty and s1 is empty it's return true
     * @param s1 scramble string
     * @param s2 string to compare
     * @return true is s1 contains s2 or false
     * Time complexity O(n) we do only addition of O(n)
     */
    fun getScramble2(s1: String, s2: String): Boolean {

        if (s2.length > s1.length) {
            return false
        }
        val s1HashMap = convertStringToHashMapCounterBetter(s1)
        val s2HashMap = convertStringToHashMapCounterBetter(s2)
        for ((key, s2Counter) in s2HashMap) {
            s1HashMap[key]?.let {
                if(it < s2Counter) return false
            } ?: run {
                return false
            }
        }
        return true

    }

    /**
     * Function who convert string to a hashMap of character and number of occurrences of this
     * character
     * @param string to convert to hashmap
     * @param s2 string to compare
     * @return true is s1 contains s2 or false
     */
    private fun convertStringToHashMapCounterBetter(string: String): HashMap<Char, Int> {
        val sHashMap = HashMap<Char, Int>()
        for (char in string) {
            sHashMap[char]?.let {
                sHashMap[char] = it + 1
            } ?: run {
                sHashMap[char] = 1
            }
        }
        return sHashMap
    }

    /**********************************************************************************************/


}