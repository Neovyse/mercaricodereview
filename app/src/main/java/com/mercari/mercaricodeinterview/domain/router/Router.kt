package com.mercari.mercaricodeinterview.domain.router

import android.content.Context
import android.content.Intent
import com.mercari.mercaricodeinterview.domain.entities.Algorithm
import com.mercari.mercaricodeinterview.presentation.scramble.ScrambleAlgorithmActivity

/***
 * Routing class used to navigate in application, View or Activity should not know what other
 * activity to open
 **/
object Router {

    fun openActivityForAlgorithm(context: Context, algorithm: Algorithm) {
        when(algorithm) {
            Algorithm.SCRAMBLE -> context.startActivity(Intent(context, ScrambleAlgorithmActivity::class.java))
        }
    }
}