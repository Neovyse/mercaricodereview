package com.mercari.mercaricodeinterview.presentation.main.algorithm

import android.view.View
import com.mercari.mercaricodeinterview.domain.entities.Algorithm
import com.mercari.mercaricodeinterview.domain.router.Router
import com.mercari.mercaricodeinterview.presentation.utils.viewmodels.ObservableViewModel

class AlgorithmItemViewModel(val algorithm: Algorithm) : ObservableViewModel() {

    fun getName() = algorithm.name

    fun openActivityForAlgorithm(view: View) {
        Router.openActivityForAlgorithm(view.context, algorithm)
    }
}