package com.mercari.mercaricodeinterview.presentation.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.mercari.mercaricodeinterview.BR
import com.mercari.mercaricodeinterview.R
import com.mercari.mercaricodeinterview.domain.entities.Algorithm
import com.mercari.mercaricodeinterview.presentation.main.algorithm.AlgorithmItemViewModel

class MainRecyclerViewAdapter(var algorithms: List<Algorithm>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_algorithm, parent, false
        )
        return AlgorithmHolder(binding)
    }

    override fun getItemCount() = algorithms.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AlgorithmHolder).binding.setVariable(
            BR.viewModel,
            AlgorithmItemViewModel(algorithms[position])
        )
    }

    inner class AlgorithmHolder(val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root)
}