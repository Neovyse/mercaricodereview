package com.mercari.mercaricodeinterview.presentation.main

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mercari.mercaricodeinterview.R
import com.mercari.mercaricodeinterview.domain.entities.Algorithm
import com.mercari.mercaricodeinterview.presentation.decorator.BottomSpaceItemDecorator

@BindingAdapter("algorithmsRetrieved")
fun algorithmsRetrieved(recyclerView: RecyclerView, algorithms: List<Algorithm>) {
    recyclerView.apply {
        layoutManager = LinearLayoutManager(recyclerView.context)
        addItemDecoration(
            BottomSpaceItemDecorator(
                recyclerView.context.resources.getDimensionPixelSize(R.dimen.list_inner_space)
            )
        )
        adapter = MainRecyclerViewAdapter(algorithms)
    }
}