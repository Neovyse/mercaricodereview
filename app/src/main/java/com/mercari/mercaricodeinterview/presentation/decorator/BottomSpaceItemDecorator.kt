package com.mercari.mercaricodeinterview.presentation.decorator

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * @param spacing spacing value to add between linear item
 * Decorator use to add space between linear item in recyclerview
 */
class BottomSpaceItemDecorator(private val spacing: Int) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.bottom = spacing

    }
}