package com.mercari.mercaricodeinterview.presentation.shared

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.databinding.BindingAdapter
import com.mercari.mercaricodeinterview.util.getActivity

@BindingAdapter("toolbarTitle")
fun toolbarTitle(toolBar: Toolbar, title: String) {
    toolBar.title = title
    toolBar.context.getActivity()?.setSupportActionBar(toolBar)
}

@BindingAdapter("visible")
fun visible(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}