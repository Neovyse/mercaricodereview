package com.mercari.mercaricodeinterview.presentation.main

import android.content.Context
import android.os.Build
import androidx.databinding.Bindable
import com.mercari.mercaricodeinterview.R
import com.mercari.mercaricodeinterview.domain.usecases.GetAlgorithmsUseCase
import com.mercari.mercaricodeinterview.presentation.utils.viewmodels.ObservableViewModel

class MainActivityViewModel : ObservableViewModel() {

    fun getTitle(context: Context): String {
        return context.getString(R.string.app_name)
    }

    private var useCase = GetAlgorithmsUseCase()

    @Bindable
    fun getAlgorithms() = useCase.getAlgorithms()

}