package com.mercari.mercaricodeinterview.presentation.scramble

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.mercari.mercaricodeinterview.BR
import com.mercari.mercaricodeinterview.R
import com.mercari.mercaricodeinterview.presentation.main.MainActivityViewModel

class ScrambleAlgorithmActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ViewDataBinding>(this, R.layout.activity_scramble)
        val viewModel = ViewModelProvider(this).get(ScrambleAlgorithmViewModel::class.java)
        binding.setVariable(BR.viewModel, viewModel)
    }
}
