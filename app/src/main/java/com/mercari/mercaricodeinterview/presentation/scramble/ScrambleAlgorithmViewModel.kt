package com.mercari.mercaricodeinterview.presentation.scramble

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import androidx.databinding.Bindable
import com.mercari.mercaricodeinterview.BR
import com.mercari.mercaricodeinterview.R
import com.mercari.mercaricodeinterview.domain.usecases.GetScrambleUseCase
import com.mercari.mercaricodeinterview.presentation.utils.viewmodels.ObservableViewModel


class ScrambleAlgorithmViewModel : ObservableViewModel() {

    private val useCase = GetScrambleUseCase()
    private var result = ""
    private var stringOne = ""
    private var stringTwo = ""
    fun getTitle(context: Context): String {
        return context.getString(R.string.scramble_activity_title)
    }

    var stringOneWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun afterTextChanged(s: Editable) {
            stringOne = s.toString()
        }
    }

    var stringTwoWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun afterTextChanged(s: Editable) {
            stringTwo = s.toString()
        }
    }

    @Bindable
    fun getResult() = "Result: $result"

    fun scramble() {
        result = useCase.getScramble(stringOne, stringTwo).toString()
        notifyPropertyChanged(BR.result)
    }

}